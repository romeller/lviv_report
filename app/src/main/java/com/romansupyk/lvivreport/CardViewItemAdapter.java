package com.romansupyk.lvivreport;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;


public class CardViewItemAdapter extends RecyclerView.Adapter<CardViewItemAdapter.CardViewHolder> {

    private List<CardViewItem> cardViewItemList;


    static class CardViewHolder extends RecyclerView.ViewHolder {
        ImageView avatar, image_hero;
        TextView username, date, description;


        public CardViewHolder(View view) {
            super(view);
            avatar = view.findViewById(R.id.img_user_logo);
            image_hero = view.findViewById(R.id.img_hero);
            username = view.findViewById(R.id.text_username);
            date = view.findViewById(R.id.text_time);
            description = view.findViewById(R.id.text_complaint);

        }
    }

    public CardViewItemAdapter(List<CardViewItem> cardViewItemList) {
        this.cardViewItemList = cardViewItemList;
    }

    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.news_item_layout, parent, false);

        return new CardViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CardViewHolder holder, int position) {
        CardViewItem cardViewItem = cardViewItemList.get(position);
        holder.avatar.setImageResource(cardViewItem.getAvatar());
        holder.image_hero.setImageResource(cardViewItem.getImage_hero());
        holder.date.setText(cardViewItem.getDate());
        holder.username.setText(cardViewItem.getUsername());
        holder.description.setText(cardViewItem.getDescription());
    }

    @Override
    public int getItemCount() {
        return cardViewItemList.size();
    }
}