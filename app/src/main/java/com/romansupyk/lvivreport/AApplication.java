package com.romansupyk.lvivreport;

import android.annotation.SuppressLint;
import android.app.Application;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;

public class AApplication extends Application {

    private GoogleSignInClient mGoogleSignInClient;

    @SuppressLint("RestrictedApi")
    @Override
    public void onCreate() {
        super.onCreate();

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    public GoogleSignInClient getSignInClient() {
        return mGoogleSignInClient;
    }
}
