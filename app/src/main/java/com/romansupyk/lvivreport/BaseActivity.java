package com.romansupyk.lvivreport;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by zephyr on 3/29/18.
 */

public class BaseActivity extends AppCompatActivity {

    protected AApplication mApp;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mApp = ((AApplication)getApplication());

    }

    public void replaceFragment(Fragment fragment, int container){
        replaceFragment(fragment, container, false);
    }

    public void replaceFragment(Fragment fragment, int container, boolean addToBackStack){
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(container, fragment);
        transaction.addToBackStack(addToBackStack ? fragment.getClass().getName() : null);
        transaction.commit();
    }
}
