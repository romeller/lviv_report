package com.romansupyk.lvivreport;

import android.graphics.drawable.Drawable;

public class CardViewItem {

    private int avatar, image_hero;
    private String username, date, description;

    public CardViewItem() {
    }

    public CardViewItem(int avatar, int image_hero,
                        String username, String date, String description) {
        this.avatar = avatar;
        this.image_hero = image_hero;
        this.username = username;
        this.date = date;
        this.description = description;
    }

    public int getAvatar() {
        return avatar;
    }

    public void setAvatar(int avatar) {
        this.avatar = avatar;
    }

    public int getImage_hero() {
        return image_hero;
    }

    public void setImage_hero(int image_hero) {
        this.image_hero = image_hero;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
